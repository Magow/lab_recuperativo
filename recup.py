#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import json

def analis_json():#extracción de datos del archivo para su análisis

    with open("iris.json" ) as file:
        datos = json.load(file)

    return datos

def tratamiento(datos):

    datos_diccionario = {"flowers":''}#creación de diccionario
                                      #key = sepalo ya que los datos son características
                                     #de variados sepalos por lo que el valor = los datos de json
    datos_diccionario["flowers"] = datos

    return datos_diccionario

def contar_especies(datos_diccionario):#extracción de las flores sin repeticiones
    all_species = []
    final_species = []

    for flower in datos_diccionario['flowers']:

        if flower['species'] not in all_species:
            all_species.append(flower['species'])
        else:
            if flower['species'] not in final_species:
                final_species.append(flower['species'])

    return final_species

def muestra(datos_diccionario):#mostrar las flores

    print("las especies de flores son:")
    temp = 0
    for i in total_especies:
        temp += 1
        print("especie n°", temp,":", " ", i, sep = "" )
    print("--------------------------------------------------------------------------------")

def promedio_petalos(datos_diccionario, total_especies):# crear dos listas de valores
    promedio_largo = []
    promedio_ancho = []

    #print(promedio_altura) verificar que las listas ed promedio presentan
    #el mismo tamaño que la cantidad de especies por si fueran más ya que uno no sabe cuantas son
    #en teoría

    for i in total_especies:
        promedio_largo.append(0)
        promedio_ancho.append(0)
    #agregar a cada lista los valores de las dimensiones sumados por especie para luego
    #sacar promedios
    for flower in datos:
        for specie in range(len(total_especies)):
            if flower['species'] == total_especies[specie]:
                promedio_largo[specie] += flower['petalLength']
                promedio_ancho[specie] += flower['petalWidth']
    print("los promedios de altura y anchura por especies son:")
    for i in range(len(total_especies)):
        promedio_largo[i] = promedio_largo[i]/50
        promedio_ancho[i] = promedio_ancho[i]/50
        print(total_especies[i], ": altura = ", promedio_largo[i]," y anchura = ",promedio_ancho[i], sep = "")
    print("--------------------------------------------------------------------------------")
    return (promedio_largo, promedio_ancho)
#usando los promedios se extraen los máximos para cada especie
def maximos(datos_diccionario, total_especies, promedio_a, promedio_l):
    max_a = max(promedio_a)
    max_l = max(promedio_l)


    for i in range(len(total_especies)):
        if max_a == promedio_a[i]:
            print("la especie con máximo promedio de anchura es" ": ", total_especies[i], " = ", max_a, sep = "")
        if max_l == promedio_l[i]:
            print("la especie con máximo promedio de altura es" ": ", total_especies[i]," = ", max_l, sep = "" )
    print("--------------------------------------------------------------------------------")

def rangos_50(datos_diccionario, total_especies, promedio_a , promedio_l):

    especies_rango_a = []
    especies_rango_l = []

    for i in range(len(total_especies)):
        especies_rango_a.append(0)
        especies_rango_l.append(0)
#funcion para extraer rangos basada en los promedios de los anchos y largo_sepalos
# aplicando las medidas del rango (+- 3)

    for flower in datos:
        for specie in range(len(total_especies)):
            if flower['species'] == total_especies[specie]:
                if flower['petalWidth'] > (promedio_a[specie] - 3) and flower['petalWidth'] < (promedio_a[specie] + 3):
                    especies_rango_a[specie] += 1
                if flower['petalLength'] > (promedio_l[specie] - 3) and flower['petalLength'] < (promedio_l[specie] + 3):
                    especies_rango_l[specie] += 1

    for i in range(len(total_especies)):
        print("para la especie",total_especies[i], "hay", especies_rango_a[i], "valores en el rango definido para su ancho")
        print("para la especie",total_especies[i], "hay", especies_rango_l[i], "valores en el rango definido para su largo")

    print("--------------------------------------------------------------------------------")
#extracción del máximo valor para el largo de los sepalos e identificación de la especie
#a la que pertenece
def max_sepalo(datos_diccionario, total_especies):
    sepalos = []
    especies = []
    for flower in datos:
        sepalos.append(flower['sepalLength'])
        especies.append(flower['species'])

    max_sep = max(sepalos)

    for i in range(len(especies)):
        if max_sep == sepalos[i]:
            print("la especie con el máximo tamaño de altura de sepalos es: ",especies[i], " = ", sepalos[i], sep = "" )
    print("--------------------------------------------------------------------------------")

def jsonmaker(datos_diccionario, total_especies):
    convert_json = []


    print(total_especies)

    for i in range(len(total_especies)):
        for flower in datos:
            if flower['species'] == total_especies[i]:
                convert_json.append(flower)
    json.dumps(convert_json)


    print(len(convert_json))

    print("--------------------------------------------------------------------------------")
if __name__ == "__main__":
    datos = analis_json()
    datos_diccionario = tratamiento(datos)
    total_especies = contar_especies(datos_diccionario)
    mostrar_especies = muestra(datos_diccionario)
    (promedio_a ,promedio_l) = promedio_petalos(datos_diccionario, total_especies)
    prom_maximos = maximos(datos_diccionario, total_especies, promedio_a, promedio_l)
    rangos = rangos_50(datos_diccionario, total_especies, promedio_a, promedio_l)
    max_sepalo = max_sepalo(datos_diccionario, total_especies)
    especies_json = jsonmaker(datos_diccionario, total_especies)
